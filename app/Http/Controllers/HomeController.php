<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** @var User $user */
        $user  = Auth::user();
        $posts = Post::where('user_id', $user->id)->get()->sortByDesc('updated_at');

        return view('home', ['posts' => $posts]);
    }

    public function edit(Post $post) {
        return view('edit-post', ['post' => $post]);
    }

    public function save(Post $post) {
        $title = Input::get('title');
        $content = Input::get('content');

        $post->title = $title;
        $post->content = $content;
        $post->save();

        return redirect()->route('edit-post', ['post' => $post->id])->with('status', 'Saved successfully!');
    }

    public function new() {
        return view('new-post');
    }

    public function create() {
        $title = Input::get('title');
        $content = Input::get('content');
        $user  = Auth::user();

        $post = new Post();

        $post->title = $title;
        $post->content = $content;
        $post->user_id = $user->id;
        $post->save();

        return redirect()->route('edit-post', ['post' => $post->id])->with('status', 'Saved successfully!');
    }
}
