@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3>All Posts</h3>

                    <ul>
                    @foreach($posts as $post)
                        <li>
                            <a href="{{ route('edit-post', ['post' => $post->id]) }}">{{ $post->title }}</a>
                        </li>
                    @endforeach
                    </ul>

                        <a href="{{ route('new-post') }}" class="btn btn-primary">New Post</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
