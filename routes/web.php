<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/edit/{post}', 'HomeController@edit')->name('edit-post');
Route::post('/edit/{post}', 'HomeController@save')->name('save-post');
Route::get('/new', 'HomeController@new')->name('new-post');
Route::post('/new', 'HomeController@create')->name('create-post');